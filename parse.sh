#!/usr/bin/env bash

parser=$(docker ps | grep wiki_parser | awk '{print $1}')
docker cp $1 ${parser}:/home/dumps/$(basename $1)

echo "$1 sent to parser"
