#!/usr/bin/env bash

for service in $(ls services); do
	pushd services/${service}
	docker build -t bwroblewski/${service}:wiki .
	docker push bwroblewski/${service}:wiki
	popd
done
