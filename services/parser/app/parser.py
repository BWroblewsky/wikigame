import collections
import re

from xml.etree import ElementTree


class WikiParser:
    def __init__(self, lang, filename):
        self.lang = lang
        self.filename = filename

        self.pages = set()
        self.redirects = dict()

        self.links = collections.defaultdict(list)

    def parse(self, function):
        parser = ElementTree.iterparse(self.filename)

        for _, element in parser:
            function(element)

    def add_page(self, element):
        if self._is_page(element):
            self.pages.add(self._get_title(element))

    def add_redirect(self, element):
        if self._is_page(element) and self._is_redirect(element) and self._get_redirect(element) in self.pages:
            self.redirects[self._get_title(element)] = self._get_redirect(element)

    def add_link(self, element):
        if self._is_page(element):
            title = self._get_title(element)
            self.links[title] = list(filter(self.pages.__contains__, self._get_links(element)))

    def _get_namespace(self, element):
        return re.match(r'{\S+}', element.tag).group(0)

    def _is_page(self, element):
        return re.fullmatch(r'{\S+}page', element.tag)

    def _get_title(self, element):
        return element.find(f'{self._get_namespace(element)}title').text

    def _is_redirect(self, element):
        return element.find(f'{self._get_namespace(element)}redirect') is not None

    def _get_redirect(self, element):
        return element.find(f'{self._get_namespace(element)}redirect').attrib['title']

    def _get_links(self, element):
        namespace = self._get_namespace(element)
        text = element.find(f'{namespace}revision').find(f'{namespace}text').text

        for link in re.findall(r'\[\[[^\[\]]+\]\]', text):
            link = link.replace('[', '').replace(']', '').split('|')[0]
            yield self.redirects.get(link, link)
