import sys

from app.parser import WikiParser


if __name__ == '__main__':
    wiki_parser = WikiParser(*sys.argv[1:])

    wiki_parser.parse(wiki_parser.add_page)
    print(len(wiki_parser.pages))

    wiki_parser.parse(wiki_parser.add_redirect)
    print(len(wiki_parser.redirects))

    wiki_parser.parse(wiki_parser.add_link)
    print(sum(len(links) for links in wiki_parser.links.values()))
